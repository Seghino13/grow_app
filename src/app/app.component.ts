import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Catálogo', url: '/catalog', icon: 'bicycle' },
    { title: 'Alquilar', url: '/rent', icon: 'bag-remove' },
    { title: 'Historial', url: '/catalog/history', icon: 'file-tray-full' }
  ];
  constructor() {}
}
