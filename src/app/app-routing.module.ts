import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './layout/index/index.component';
import { Pages } from './utils/enum/pages';
import { Urls } from './utils/enum/urls';

const routes: Routes = [
  {
    path: '',
    redirectTo: Urls.catalog,
    pathMatch: 'full'
  },
  {
    path: Urls.catalog,
    loadChildren: () => import('./pages/catalog/catalog/catalog.module').then( m => m.CatalogModule),
    component: IndexComponent,
    data:{
      page: Pages.catalog
    }
  },
  {
    path: Urls.rent,
    loadChildren: () => import('./pages/rent/rent.module').then( m => m.RentModule),
    component: IndexComponent,
    data:{
      page: Pages.rent
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
