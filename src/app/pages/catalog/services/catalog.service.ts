import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ICatalog } from 'src/app/models/Icatalog';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  
  constructor(private http: HttpClient) { }

  getCatalog(){
    return this.http.get<ICatalog[]>(environment.mock + '/catalog');
  }
}
