import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogListComponent } from './list/catalog-list/catalog-list.component';
import { CatalogService } from '../services/catalog.service';
import { HttpClientModule } from '@angular/common/http';
import { CatalogDetailComponent } from './catalog-detail/catalog-detail.component';
import { HistoryComponent } from './history/history.component';
import { HistoryService } from 'src/app/utils/services/history.service';
//import { IonicStorageModule } from '@ionic/storage'

@NgModule({
  declarations: [CatalogListComponent, CatalogDetailComponent, HistoryComponent],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    HttpClientModule,
    //IonicStorageModule.forRoot()
  ],
  providers:[ CatalogService , HistoryService,]
})
export class CatalogModule { }
