import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICatalog } from 'src/app/models/Icatalog';
import { CatalogService } from '../../../services/catalog.service';

@Component({
  selector: 'app-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss'],
  providers:[  ]
})
export class CatalogListComponent implements OnInit {
  size:number = 12;
  catalog:any;
  catalogComplete: ICatalog[] = [];
  categories = [];
  constructor(private catalogService: CatalogService, private router:Router) { }

  ngOnInit() {
    this.loadCatalog();
  }
  showAs( grid: number ) {
    this.size = grid;
  }
  loadCatalog(){
   this.catalogService.getCatalog().subscribe( data =>{
      this.catalog = data;
      this.catalogComplete = data;
      this.filterByCategory(this.catalog);
    },err=>{
      console.log( err );
    }) 
  }
  filterByCategory( catalogData){
    catalogData.forEach( catalog =>{ 
      const cat = this.categories.find( item => item === catalog.category );
      if( cat === undefined ){
        this.categories.push(catalog.category);
      }
    });
  }

  filter( e ){
    console.log( e.detail.value);
    this.catalog = this.catalogComplete;
    const filter  = this.catalog.filter( item => item.category === e.detail.value);
    this.catalog =  filter ;
  }
  openDeatil( id:number){
    this.router.navigate(['/catalog/detail/'+id]);
  }
  


}
