import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogDetailComponent } from './catalog-detail/catalog-detail.component';
import { HistoryComponent } from './history/history.component';
import { CatalogListComponent } from './list/catalog-list/catalog-list.component';

const routes: Routes = [
  {
  path:'',
  component: CatalogListComponent
  },
  {
    path:'detail/:id',
    component: CatalogDetailComponent,
    data:[{childPage:'detalle'}]
  },
  {
    path:'history',
    component: HistoryComponent,
    data:[{childPage:'Historial'}]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
