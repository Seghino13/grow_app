import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICatalog } from 'src/app/models/Icatalog';
import { HistoryService } from 'src/app/utils/services/history.service';
import { CatalogService } from '../../services/catalog.service';

@Component({
  selector: 'app-catalog-detail',
  templateUrl: './catalog-detail.component.html',
  styleUrls: ['./catalog-detail.component.scss'],
  providers:[ CatalogService, HistoryService ]
})
export class CatalogDetailComponent implements OnInit {
  id;
  catalog:ICatalog;
  constructor(private activatedRoute: ActivatedRoute, 
              private router:Router, 
              private catalogService:CatalogService,
              private historyService:HistoryService,) { }

  ngOnInit() {
    const id:number = parseInt( this.activatedRoute.snapshot.paramMap.get('id') );
    this.getDataAndFilterById( id );
    
  }

  getDataAndFilterById( id:number ){
    this.catalogService.getCatalog().subscribe( data=>{
        this.catalog = data.find( item => item.id === id);
        this.saveInHistory(this.catalog);
    }); 
  }

  saveInHistory( catalog: ICatalog ){
    this.historyService.saveHistory( catalog );
  }

}
