import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICatalog } from 'src/app/models/Icatalog';
import { HistoryService } from 'src/app/utils/services/history.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers:[HistoryService]
})
export class HistoryComponent implements OnInit {
  history:ICatalog;
  constructor(private historyService:HistoryService, private router:Router) { }

  ngOnInit() {
    this.getHistoryData();
  }
  getHistoryData(){
   this.history =  JSON.parse( this.historyService.getHistory() );
  }
  openDeatil( id:number){
    this.router.navigate(['/catalog/detail/'+id]);
  }
}
