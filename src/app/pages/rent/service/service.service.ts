import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IRent } from 'src/app/models/IRent';

@Injectable({
  providedIn: 'root'
})
export class ServiceRent {

  constructor(private http: HttpClient) { }

  createRent( request: IRent ){
    return this.http.post<IRent>(environment.mock + '/createRent', request );
  }
}
