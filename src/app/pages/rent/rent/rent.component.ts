import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl,  FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IRent } from 'src/app/models/IRent';
import { UtilsService } from 'src/app/utils/utils/utils.service';
import { CatalogService } from '../../catalog/services/catalog.service';
import { ServiceRent } from '../service/service.service';
@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.scss'],
  providers: [CatalogService, ServiceRent, UtilsService]
})
export class RentComponent implements OnInit {

  categories = [];
  rent: FormGroup;
  submitted = false;
  constructor( public formBuilder: FormBuilder, 
              private catalogService:CatalogService,
              private serviceRent:ServiceRent,
              private utilsService:UtilsService,
              private router:Router ) { }

  ngOnInit() {
    this.loadCatalog();
    this.loadForm();
  }
  loadForm(){
    this.rent = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      category: ['', [Validators.required, Validators.minLength(3)]],
      startDate: ['', [Validators.required, Validators.minLength(3)]],
      endDate: ['', [Validators.required, Validators.minLength(3)]],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
   
  }

  loadCatalog(){
    this.catalogService.getCatalog().subscribe( data =>{
       this.categories = data;
        console.log( this.categories  );
     },err=>{
       console.log( err );
     }) 
   }

  get errorCtr() {
    return this.rent.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.rent.valid) {
      this.utilsService.toast('Oopps algo paso, por favor valida el formulario');
      return false;
    } else {     
      const req: IRent = this.rent.value;
      this.serviceRent.createRent( req ).subscribe( res =>{
        this.utilsService.toast('Se ha alquilado exitosamente');
        this.rent.reset();
        this.router.navigate(['/catalog']);
      }, err =>{
        console.log( err );
      })
    }
  }

}
