import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RentRoutingModule } from './rent-routing.module';
import { RentComponent } from './rent/rent.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';
import { ServiceRent } from './service/service.service';

@NgModule({
  declarations: [RentComponent],
  imports: [
    CommonModule,
    RentRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    IonicModule
  ],
  providers:[ ServiceRent ]
})
export class RentModule { }
