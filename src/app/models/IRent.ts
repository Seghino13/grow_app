export interface IRent{
    firstName:String;
    lastName:String;
    email:String;
    category:Number;
    startDate:Date;
    endDate:Date;
    phone:String;
}

