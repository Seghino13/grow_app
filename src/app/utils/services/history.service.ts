import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ICatalog } from 'src/app/models/Icatalog';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(private platform:Platform, 
    //private storage:Storage
     ) { }

  getHistory(){
    if (this.platform.is("cordova")) {
    } else {
      return localStorage.getItem("history");
    }
  }

  saveHistory( catalog: ICatalog ){

    const history:any  = JSON.parse(this.getHistory());
    
    
    if( !history  ){
      this.saveIn( JSON.stringify( [catalog] ) );
    }else{
      const findTmp = history.find( item => catalog.id === item.id );
      if( findTmp === undefined ){
        history.push( catalog );
        this.saveIn( JSON.stringify( history ) );
      }
    }
  }

  saveIn( history ) {
    if (this.platform.is("cordova")) {
     /*  this.storage.setItem('history', history);
      this.storage.removeItem('history'); */
    } else {
      localStorage.removeItem('history');
      localStorage.setItem('history', history);
    }
  }

}
